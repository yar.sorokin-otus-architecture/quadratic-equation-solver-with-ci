using AutoFixture;
using FluentAssertions;
using Quadratic.Equation.Solver;
using Xunit;

namespace Quadriatic.Equation.Tests;

public class SolverTests : IClassFixture<TestFixture>
{
    private readonly Fixture _fixture;
    private readonly Solver _solver;
    
    public SolverTests(TestFixture testFixture)
    {
        _fixture = testFixture.Fixture;
        _solver = testFixture.Solver;
    }
    
    [Fact]
    public void Validate_Returns_Empty_If_A_Equals_Zero()
    {
        //Arrange
        var a = 0.0;
        var b = _fixture.Create<double>();
        var c = _fixture.Create<double>();
        
        //Act
        var result = _solver.Solve(a, b, c);
        
        //Assert
        result.Should().HaveCount(0);
    }
    
    [Fact]
    public void Validate_Returns_Empty_If_A_Equals_One_B_Equals_Zero_C_Equals_One()
    {
        //Arrange
        var a = 1.0;
        var b = 0.0;
        var c = 1.0;
        
        //Act
        var result = _solver.Solve(a, b, c);
        
        //Assert
        result.Should().HaveCount(0);
    }
    
    [Fact]
    public void Validate_Returns_One_And_NegativeOne_If_A_Equals_One_B_Equals_Zero_C_Equals_NegativeOne()
    {
        //Arrange
        var a = 1.0;
        var b = 0.0;
        var c = -1.0;
        
        //Act
        var result = _solver.Solve(a, b, c);
        
        //Assert
        result.Should().HaveCount(2);
        result[0].Should().Be(1.0);
        result[1].Should().Be(-1.0);
    }
    
    [Fact]
    public void Validate_Returns_NegativeOne_And_NegativeOne_If_A_Equals_One_B_Equals_Two_C_Equals_One()
    {
        //Arrange
        var a = 1.0;
        var b = 2.0;
        var c = 1.0;
        
        //Act
        var result = _solver.Solve(a, b, c);
        
        //Assert
        result.Should().HaveCount(2);
        result[0].Should().Be(-1.0);
        result[0].Should().Be(result[1]);
    }
    
    [Fact]
    public void Validate_Returns_Exception_If_A_Equals_NaN()
    {
        //Arrange
        var a = double.NaN;
        var b = _fixture.Create<double>();
        var c = _fixture.Create<double>();
        
        //Act
        var act = () => _solver.Solve(a, b, c);

        //Assert
        Assert.Throws<ArgumentException>(act);
    }
    
    [Fact]
    public void Validate_Returns_Exception_If_B_Equals_NegativeInf()
    {
        //Arrange
        var a = _fixture.Create<double>();
        var b = double.NegativeInfinity;
        var c = _fixture.Create<double>();
        
        //Act
        var act = () => _solver.Solve(a, b, c);

        //Assert
        Assert.Throws<ArgumentException>(act);
    }
    
    [Fact]
    public void Validate_Returns_Exception_If_C_Equals_PositiveInf()
    {
        //Arrange
        var a = _fixture.Create<double>();
        var b = _fixture.Create<double>();
        var c = double.PositiveInfinity;
        
        //Act
        var act = () => _solver.Solve(a, b, c);

        //Assert
        Assert.Throws<ArgumentException>(act);
    }
}