using AutoFixture;
using Quadratic.Equation.Solver;

namespace Quadriatic.Equation.Tests;

public class TestFixture : IDisposable
{
    public Fixture Fixture { get; }
    public Solver Solver { get; }
    public TestFixture()
    {
        Fixture = new Fixture();
        Solver = new Solver();
    }

    public void Dispose()
    {
    }
}