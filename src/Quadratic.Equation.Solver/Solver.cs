namespace Quadratic.Equation.Solver;

public class Solver
{
    public double[] Solve(double a, double b, double c, double e = 1e-10)
    {
        if (double.IsNaN(a) || double.IsNegativeInfinity(a) || double.IsPositiveInfinity(a) ||
        double.IsNaN(b) || double.IsNegativeInfinity(b) || double.IsPositiveInfinity(b) ||
        double.IsNaN(c) || double.IsNegativeInfinity(c) || double.IsPositiveInfinity(c) ||    
        double.IsNaN(e) || double.IsNegativeInfinity(e) || double.IsPositiveInfinity(e))
        {
            throw new ArgumentException();
        }
        // a=0
        if (Math.Abs(a) <= e)
            return Array.Empty<double>();
        
        var discriminant = b * b - 4 * a * c;
        
        //discriminant < 0
        if (discriminant < -e)
        {
            return Array.Empty<double>();
        }
        
        //discriminant = 0
        if (Math.Abs(discriminant) <= e)
            discriminant = 0;
        
        return new double[] {
            (-b+Math.Sqrt(discriminant))/(2*a), 
            (-b-Math.Sqrt(discriminant))/(2*a) 
        };
    }
}